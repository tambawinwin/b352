{{-- Activity s02 --}}

@extends('layouts.app')

@section('content')

<div class="container mt-4">
    <div class="row justify-content-center align-items-center"> 
        <div class="col-md-8 text-center">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png" alt="Laravel Logo" class="img-fluid mt-4 mb-4" style="max-width: 100%;">
        </div>
    </div>
    <h2 class="text-center mt-4">Featured Posts:</h2>
    <div class="col text-center justify-content-center align-items-center">
        @foreach($featuredPosts as $post)
        <div class="row-md-4">
            <div class="card mb-4">
                <div class="card-body">
                    <h5 class="card-title">
                        <a href="/posts/{{$post->id}}">{{$post->title}}</a>
                    </h5>
                    <p class="card-text">Author: {{$post->user->name}}</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
