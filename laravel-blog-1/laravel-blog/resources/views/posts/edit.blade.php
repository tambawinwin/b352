{{-- 
s03 activity
Create the view (edit.blade.php) being returned by the above controller action in the posts subdirectory of the views folder and have it show a form where both title and content may be submitted via PUT request to the /post/{id} endpoint.
To add the method put in the form use the @method(‘PUT’) directives
 --}}

 @extends('layouts.app')

 @section('content')
     <div class="container mt-4">
         <h2>Edit Post</h2>
         <form method="POST" action="/posts/{{$post->id}}">
             @method('PUT')
             @csrf
             <div class="form-group">
                 <label for="title">Title:</label>
                 <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
             </div>
             <div class="form-group">
                 <label for="content">Content:</label>
                 <textarea class="form-control" id="content" name="content" rows="3">{{$post->content}}</textarea>
             </div>
             <div class="mt-2">
                 <button type="submit" class="btn btn-primary">Update Post</button>
             </div>
         </form>
     </div>
 @endsection
