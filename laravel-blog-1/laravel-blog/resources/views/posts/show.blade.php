@extends('layouts.app')

@section('content')
	<div class="card">
		<div class="card-body">
			<h2 class="card-title">{{$post->title}}</h2>
			<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
			<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
			<p class="card-text">{{$post->content}}</p>
			<!-- Display the number of likes & comments -->
			<p>Likes: {{ $post->likes->count() }} | Comments: {{ $post->comments->count() }}</p>
			@if(Auth::id() !=$post->user_id)
				<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
					@method('PUT')
					@csrf
					@if($post->likes->contains("user_id", Auth::id()))
						<button type="submit" class="btn btn-danger">Unlike</button>
					@else
						<button type="submit" class="btn btn-success">Like</button>
					@endif
				</form>
			@endif
			<!-- Button to trigger the modal -->
			<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#commentModal">
			    Add Comment
			</button>

			<!-- Comment Modal -->
			<div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
			    <div class="modal-dialog" role="document">
			        <div class="modal-content">
			            <div class="modal-header">
			                <h5 class="modal-title" id="commentModalLabel">Add a Comment</h5>
			                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			            </div>
			            <div class="modal-body">
			                <!-- Comment Form -->
			                <form action="/posts/{{ $post->id }}/comment" method="POST">
			                    @csrf
			                    <div class="form-group">
			                        <label for="content">Comment:</label>
			                        <textarea class="form-control" name="content" id="content" rows="4" required></textarea>
			                    </div>
			                    <button type="submit" class="btn btn-primary">Submit Comment</button>
			                </form>
			            </div>
			        </div>
			    </div>
			</div>

			<!-- Section to list comments -->
			<div class="mt-4">
			    <h4>Comments:</h4>
			    <ul class="list-group">
			        @foreach($post->comments as $comment)
			            <li class="list-group-item">
			                <h4 class="text-center">{{ $comment->content }}</h4>
			                <div class="text-end">
			                	<h5>Posted by: {{ $comment->user->name }}</h5>
			                	<h6 class="text-muted">posted on: {{ $comment->user->created_at }}</h6>
			                </div> 
			            </li>
			        @endforeach
			    </ul>
			</div>


			<div class="mt-3">
				<a href="/posts" class="card-link">View all posts</a>
			</div>
		</div>
	</div>
@endsection