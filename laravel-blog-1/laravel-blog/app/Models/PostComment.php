<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    use HasFactory;

    // Define the relationship with the Post model
    public function post(){
        return $this->belongsTo('App\Models\Post');
    }

    // Define the relationship with the User model
    public function user(){
        return $this->belongsTo('App\Models\User');
    }
}
